const chess = require('chess');

class Game {
    constructor(socket) {
        this.turn = 'w';
        this.socket = socket;
        this.game = chess.create({PGN: true});
    }

    getBoard(){
        let theBoard = Array(8).fill(null);
        for(let i = 0; i < theBoard.length; i++){
            theBoard[i] = Array(8).fill(null);
        }
        let curPiece = "";
        let gameBoard = this.game.getStatus().board.squares;
        for(let i = 0; i < gameBoard.length; i++){
            if(gameBoard[i].piece === null){
                curPiece = null;
            } else {
                if(gameBoard[i].piece.type === 'pawn'){
                    curPiece = 'P';
                } else {
                    curPiece = gameBoard[i].piece.notation;
                }
                if(gameBoard[i].piece.side.name === 'white'){
                    curPiece = curPiece + 'w';
                } else {
                    curPiece = curPiece + 'b';
                }
            }
            theBoard[theBoard.length - gameBoard[i].rank][gameBoard[i].file.charCodeAt(0) - 'a'.charCodeAt(0)] = curPiece;
        }
        return theBoard;
    }

    sendBoardToClient(event) {
        let data = {
            matchType: "OTB",
            theBoard: this.getBoard()
        };
        this.socket.emit(event, data);
    }

    onlineCount() {
        let data = {
            onlineCount: 0,
        };
        this.socket.emit('onlineCount', data);
    }

    initGame() {
        console.log("Initializing new game");
        this.sendBoardToClient('init');
    }

    makeMove(startingCoordinate, endingCoordinate) {
        console.log("Move requested");
        let isValidMove = this._isValidMove(startingCoordinate, endingCoordinate);
        console.log(isValidMove);
        if(isValidMove === null){
            console.log("Trying to make invalid move");
            return;
        }
        this.game.makeMove(isValidMove);
        this.sendBoardToClient("move");
    }

    _isValidMove(startingCoordinate, endingCoordinate) {
        let notatedMoves = this.game.getStatus().notatedMoves;

        let curSrcFile;
        let curSrcRank;
        let curDestFile;
        let curDestRank;
        for(let move in notatedMoves) {
            if(Object.prototype.hasOwnProperty.call(notatedMoves, move)){
                curSrcFile = notatedMoves[move].src.file;
                curSrcRank = notatedMoves[move].src.rank;
                curDestFile = notatedMoves[move].dest.file;
                curDestRank = notatedMoves[move].src.rank;

                console.log("StartFile: " + startingCoordinate.right);
                console.log("StartRank: " + startingCoordinate.down);
                console.log("curSrcFile: " + curSrcFile);
                console.log("curSrcRank: " + curSrcRank);

                if(startingCoordinate.right === curSrcFile.charCodeAt(0) - 'a'.charCodeAt(0) &&
                    startingCoordinate.down === 8 - curSrcRank &&
                    endingCoordinate.right === curDestFile.charCodeAt(0) - 'a'.charCodeAt(0) &&
                    endingCoordinate.down === curDestRank){
                        console.log("Valid move");
                        return move;
                    }
            }
        }
        return null; 
    }

    //TODO: Delete this later
    _testGame() {
        this.game.move("e4");
        this.game.move("e5");
        console.log(this.game.getStatus());
        let notatedMoves = this.game.getStatus().notatedMoves;
        for(let move in notatedMoves){
            if(Object.prototype.hasOwnProperty.call(notatedMoves, move)){
                console.log(notatedMoves[move].dest.file);
                console.log(notatedMoves[move].dest.rank);
            }
        }
    }
};

module.exports = { Game }